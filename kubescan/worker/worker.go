package worker

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/golang-collections/collections/queue"
	"task"
)
type Worker struct {
	Name	string
	Queue	queue
	DB	map[uuid.UUID]*tak.Task
	TaskCount int
}

func (w *Worker) CollectStats() {
	fmt.Println("I will collect stats")
}

func (w *Worker) RunTask() {
	fmt.Println("I will start or stop a task" )
}

func (w *Worker) StartTask() {
	fmt.Println("I will start a task")
}

func (w *Worker) StopTask() {
	fmt.Println("I will stop a task")
}

func (w *Worker) SelectWorker() {
	fmt.Println("I will select an appropriate worker") 
}

func (w *Worker) UpdateTasks() {
	fmt.Println("I will update tasks")
}

func (w *Worker) SendWork() {
	fmt.Println("I will send work to workers") 
}
